<?php // $Id$
?>
<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php if ($page == 0) { ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
    <span class="submitted"><?php print $submitted?></span>
    <div class="<?php if ($teaser) { print " teaser"; } ?>">
     <div class="<?php if ($sticky) { print " sticky"; } ?>">
      <div class="content"><?php print $content?></div>
     </div>
    </div>
    <div class="clear-block clear">
     <div class="meta">
      <?php if ($links): ?>
       <div class="links"><?php print $links; ?></div>
        <?php endif; ?>
        <?php if ($taxonomy): ?>
      <div class="terms"><?php print $terms ?></div>
        <?php endif;?>
      </div>
    </div>
    <div class="cleardiv"></div>
</div>
