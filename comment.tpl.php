<?php // $Id$
?>
<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
  <h3 class="title"><?php print $title; ?></h3><?php if ($new != '') { ?><span class="new"><?php print $new; ?></span><?php } ?>
  <span class="submitted"><?php print $submitted?></span>
  <?php if ($picture) {print $picture;} ?>
  <div class="content"><?php print $content; ?></div>
  <div class="links"><?php print $links; ?></div>
</div>
