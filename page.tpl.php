<?php // $Id$
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
  <?php print $head ?>
  <title><?php print $head_title ?></title>
  <?php print $styles ?>
  <?php print $scripts ?>
  </head>
  <body>
   <div id="hauptcontainer">
   <div id="homelink"><?php if ($site_name) { ?><h1><a href="<?php print $base_path ?>"><?php print $site_name ?></a></h1><?php } ?></div>
   <div id="container-shadow-left">
    <div id="container-shadow-right">
     <div id="container-shadow-bottom-left">
      <div id="container-shadow-bottom-right">
       <div id="header">
       </div>
        <div id="top-menu">
         <?php if (isset($primary_links)) : ?>
         <?php print theme('links', $primary_links, array('class' => 'nav')) ?>
         <?php endif; ?>
        </div>
        <div id="container-weisshintergrund">
         <div id="container-gestrichelt">
          <div id="wrapper">
           <div id="content">
            <h2 class="title node-full"><?php print $title ?></h2>
            <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
            <?php print $help ?>
            <?php print $messages ?>
            <?php print $content ?>
          </div>
         </div>
        <div id="sidebar-right">
          <?php print $right ?>
        </div>
        <div class="cleardiv"></div>
       </div>
      </div>
      <div id="footer">
        Published under GPLv2. Design by <a href="http://rufzeichen-online.de"><img src="<?php print $base_path.$directory ?>/images/rufzeichen-drupal-webdesign-agentur.png " alt="drupal webdesign agentur" /> </a> use in the spirit of open source <?php print $footer_message ?><a href="http://drupal.org"> <img style="padding-left: 7px;" src=" <?php print $directory ?>/images/powered-blue-80x15.png" alt="powered by Drupal" height="13" width="78" /></a></div>
      </div>
     </div>
    </div>
    </div>
   </div>
   </div>
    <?php print $closure ?>
  </body>
</html>
